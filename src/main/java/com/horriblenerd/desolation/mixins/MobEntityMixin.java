package com.horriblenerd.desolation.mixins;

import net.minecraft.entity.MobEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

/**
 * Created by HorribleNerd on 06/09/2020
 */
@Mixin(MobEntity.class)
public class MobEntityMixin {

    /**
     * @author HorribleNerd
     * @reason Sunproof mobs
     */
    @Inject(method = "isInDaylight()Z", at = @At(value = "RETURN"), cancellable = true)
    public void onIsInDaylight(CallbackInfoReturnable<Boolean> cir) {
        cir.setReturnValue(false);
    }

}