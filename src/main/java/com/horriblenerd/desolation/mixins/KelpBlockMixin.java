package com.horriblenerd.desolation.mixins;

import net.minecraft.block.AbstractBodyPlantBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.ILiquidContainer;
import net.minecraft.block.KelpBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.loot.LootContext;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import org.spongepowered.asm.mixin.Mixin;

import java.util.Collections;
import java.util.List;

/**
 * Created by HorribleNerd on 06/09/2020
 */
@Mixin(KelpBlock.class)
public abstract class KelpBlockMixin extends AbstractBodyPlantBlock implements ILiquidContainer {

    protected KelpBlockMixin(Properties p_i241179_1_, Direction p_i241179_2_, VoxelShape p_i241179_3_, boolean p_i241179_4_) {
        super(p_i241179_1_, p_i241179_2_, p_i241179_3_, p_i241179_4_);
    }

    @Override
    public ItemStack getItem(IBlockReader worldIn, BlockPos pos, BlockState state) {
        if (pos.getY() >= 21) return ItemStack.EMPTY;
        return super.getItem(worldIn, pos, state);
    }

    @Override
    public List<ItemStack> getDrops(BlockState state, LootContext.Builder builder) {
        return Collections.singletonList(ItemStack.EMPTY);
    }

    @Override
    public Item asItem() {
        return Items.AIR;
    }
}
