package com.horriblenerd.desolation.mixins;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.BreakableBlock;
import net.minecraft.block.IceBlock;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.LightType;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import org.spongepowered.asm.mixin.Mixin;

import java.util.Random;

import static com.horriblenerd.desolation.util.BiomeUtil.isColdBiome;

/**
 * Created by HorribleNerd on 06/09/2020
 */
@Mixin(IceBlock.class)
public abstract class IceBlockMixin extends BreakableBlock {

    public IceBlockMixin(Properties properties) {
        super(properties);
    }

    @Override
    public void tick(BlockState state, ServerWorld worldIn, BlockPos pos, Random rand) {
        if (worldIn.isPlayerWithin(pos.getX(), pos.getY(), pos.getZ(), 128) && shouldMelt(state, worldIn, pos)) {
            turnIntoWater(state, worldIn, pos);
        }
    }

    @Override
    public void randomTick(BlockState state, ServerWorld worldIn, BlockPos pos, Random random) {
        if (shouldMelt(state, worldIn, pos)) {
            turnIntoWater(state, worldIn, pos);
        }
    }

    public boolean shouldMelt(BlockState state, World worldIn, BlockPos pos) {
        return worldIn.getLightFor(LightType.BLOCK, pos) > 14 - state.getOpacity(worldIn, pos)
                || (worldIn.isDaytime() && ((worldIn.getBlockState(pos.up()).getBlock() != Blocks.ICE) || worldIn.isAirBlock(pos.down())) && !isColdBiome(worldIn, pos));
    }

    @Override
    public boolean propagatesSkylightDown(BlockState state, IBlockReader reader, BlockPos pos) {
        return false;
    }

    protected void turnIntoWater(BlockState state, World worldIn, BlockPos pos) {
        if (worldIn.func_230315_m_().func_236040_e_() || worldIn.isDaytime()) {
            worldIn.removeBlock(pos, false);
        } else {
            worldIn.setBlockState(pos, Blocks.WATER.getDefaultState());
            worldIn.neighborChanged(pos, Blocks.WATER, pos);
        }
    }



}
