package com.horriblenerd.desolation.mixins;

import net.minecraft.world.GrassColors;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

/**
 * Created by HorribleNerd on 07/09/2020
 */
@Mixin(GrassColors.class)
public class GrassColorsMixin {

    @Shadow
    private static int[] grassBuffer;

    /**
     * @author HorribleNerd
     */
    @Overwrite
    public static int get(double temperature, double humidity) {
        // -35281: Orange
        // -25281: Dead orange
        // -15281: Very dead
        return (int) (-15281 - 2000 * temperature + 2000 * humidity);
    }

}
