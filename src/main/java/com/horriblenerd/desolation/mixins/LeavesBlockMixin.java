package com.horriblenerd.desolation.mixins;

import net.minecraft.block.BlockState;
import net.minecraft.block.LeavesBlock;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.server.ServerWorld;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Random;

/**
 * Created by HorribleNerd on 07/09/2020
 */
@Mixin(LeavesBlock.class)
public abstract class LeavesBlockMixin {

    @Shadow
    @Final
    public static IntegerProperty DISTANCE = BlockStateProperties.DISTANCE_1_7;

    @Shadow
    @Final
    public static BooleanProperty PERSISTENT = BlockStateProperties.PERSISTENT;

    @Inject(method = "randomTick(Lnet/minecraft/block/BlockState;Lnet/minecraft/world/server/ServerWorld;Lnet/minecraft/util/math/BlockPos;Ljava/util/Random;)V", at = @At("RETURN"))
    private void onRandomTick(BlockState state, ServerWorld worldIn, BlockPos pos, Random random, CallbackInfo ci) {
        if (pos.getY() >= 21) worldIn.removeBlock(pos, false);
    }

    /**
     * @author HorribleNerd
     * @reason Make all natural leaves decay
     */
    @Overwrite
    public boolean ticksRandomly(BlockState state) {
        return !state.get(PERSISTENT);
    }

}
