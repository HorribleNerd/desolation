package com.horriblenerd.desolation.mixins;

import net.minecraft.block.*;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.loot.LootContext;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import org.spongepowered.asm.mixin.Mixin;

import java.util.Collections;
import java.util.List;

/**
 * Created by HorribleNerd on 06/09/2020
 */
@Mixin(KelpTopBlock.class)
public abstract class KelpTopBlockMixin extends AbstractTopPlantBlock implements ILiquidContainer {


    protected KelpTopBlockMixin(Properties p_i241180_1_, Direction p_i241180_2_, VoxelShape p_i241180_3_, boolean p_i241180_4_, double p_i241180_5_) {
        super(p_i241180_1_, p_i241180_2_, p_i241180_3_, p_i241180_4_, p_i241180_5_);
    }

    @Override
    public ItemStack getItem(IBlockReader worldIn, BlockPos pos, BlockState state) {
        if (pos.getY() >= 21) return ItemStack.EMPTY;
        return super.getItem(worldIn, pos, state);
    }

    @Override
    public List<ItemStack> getDrops(BlockState state, LootContext.Builder builder) {
        return Collections.singletonList(ItemStack.EMPTY);
    }

    @Override
    public Item asItem() {
        return Items.AIR;
    }

}
