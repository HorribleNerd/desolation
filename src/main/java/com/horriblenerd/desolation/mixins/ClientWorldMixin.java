package com.horriblenerd.desolation.mixins;

import com.horriblenerd.desolation.util.BiomeUtil;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.profiler.IProfiler;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.DimensionType;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.storage.ISpawnWorldInfo;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.function.Supplier;

/**
 * Created by HorribleNerd on 07/09/2020
 */
@Mixin(ClientWorld.class)
public abstract class ClientWorldMixin extends World {

    protected ClientWorldMixin(ISpawnWorldInfo p_i241925_1_, RegistryKey<World> p_i241925_2_, DimensionType p_i241925_3_, Supplier<IProfiler> p_i241925_4_, boolean p_i241925_5_, boolean p_i241925_6_, long p_i241925_7_) {
        super(p_i241925_1_, p_i241925_2_, p_i241925_3_, p_i241925_4_, p_i241925_5_, p_i241925_6_, p_i241925_7_);
    }

    @Inject(method = "getSkyColor(Lnet/minecraft/util/math/BlockPos;F)Lnet/minecraft/util/math/vector/Vector3d;", at = @At("RETURN"), cancellable = true)
    public void getSkyColor(BlockPos blockPosIn, float partialTicks, CallbackInfoReturnable<Vector3d> cir) {
        if (!func_230315_m_().func_241510_j_()) return; // If Nether
        boolean isDay = getDayTime() <= 13000;
        boolean isCold = BiomeUtil.isColdBiome(this, blockPosIn);


//        float f = this.func_242415_f(partialTicks);
//        float f1 = MathHelper.cos(f * ((float)Math.PI * 2F)) * 2.0F + 0.5F;
//        f1 = MathHelper.clamp(f1, 0.0F, 1.0F);
//        Biome biome = this.getBiome(blockPosIn);
//        int i = biome.getSkyColor();
//        float f2 = (float)(i >> 16 & 255) / 255.0F;
//        float f3 = (float)(i >> 8 & 255) / 255.0F;
//        float f4 = (float)(i & 255) / 255.0F;
//        f2 = f2 * f1;
//        f3 = f3 * f1;
//        f4 = f4 * f1;
        // TODO: weather

        Vector3d mult = cir.getReturnValue().crossProduct(new Vector3d(0.3F, 0.3F, 0.3F));

        cir.setReturnValue(isDay && !isCold ? new Vector3d(255, 178, 102).add(mult) : new Vector3d(127, 204, 219).add(mult));
    }


}
