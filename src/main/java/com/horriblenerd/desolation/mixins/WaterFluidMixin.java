package com.horriblenerd.desolation.mixins;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.ChestBlock;
import net.minecraft.fluid.FlowingFluid;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.WaterFluid;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.server.ServerWorld;
import org.spongepowered.asm.mixin.Mixin;

import java.util.Random;

import static com.horriblenerd.desolation.util.BiomeUtil.isColdBiome;

/**
 * Created by HorribleNerd on 06/09/2020
 */
@Mixin(WaterFluid.class)
public abstract class WaterFluidMixin extends FlowingFluid {

    @Override
    public void tick(World worldIn, BlockPos pos, FluidState state) {
        if (worldIn.isPlayerWithin(pos.getX(), pos.getY(), pos.getZ(), 128)) {
            if (shouldEvaporate(worldIn, pos, state)) {
                evaporate(worldIn, pos, state);
            }
            else if (shouldFreeze(worldIn, pos, state)) {
                freeze(worldIn, pos, state);
            }
        }
        else {
            super.tick(worldIn, pos, state);
        }
    }

    public void evaporate(World worldIn, BlockPos pos, FluidState state) {
        if (worldIn.getBlockState(pos).getBlock() == Blocks.CHEST) {
            worldIn.setBlockState(pos, worldIn.getBlockState(pos).with(ChestBlock.WATERLOGGED, false));
        }
        else {
            worldIn.setBlockState(pos, Blocks.AIR.getDefaultState());
        }
        Block down = worldIn.getBlockState(pos.down()).getBlock();
        if (down == Blocks.KELP || down == Blocks.TALL_SEAGRASS || down == Blocks.SEAGRASS) {
            worldIn.setBlockState(pos.down(), Blocks.AIR.getDefaultState());
        }
    }

    public void freeze(World worldIn, BlockPos pos, FluidState state) {
        if (worldIn.getBlockState(pos).getBlock() == Blocks.CHEST) {
            worldIn.setBlockState(pos, worldIn.getBlockState(pos).with(ChestBlock.WATERLOGGED, false));
        }
        else {
            worldIn.setBlockState(pos, Blocks.ICE.getDefaultState());
        }
        Block down = worldIn.getBlockState(pos.down()).getBlock();
        if (down == Blocks.KELP || down == Blocks.TALL_SEAGRASS || down == Blocks.SEAGRASS) {
            worldIn.setBlockState(pos.down(), Blocks.ICE.getDefaultState());
        }
    }

    public boolean shouldEvaporate(World worldIn, BlockPos pos, FluidState state) {
        return worldIn instanceof ServerWorld && worldIn.isDaytime() && pos.getY() >= 21 && !isColdBiome(worldIn, pos);
    }

    public boolean shouldFreeze(World worldIn, BlockPos pos, FluidState state) {
        return ((worldIn instanceof ServerWorld && worldIn.isNightTime()) || isColdBiome(worldIn, pos)) && pos.getY() >= 21 && worldIn.getBlockState(pos.down()).getBlock() != Blocks.AIR;
    }

    @Override
    public void randomTick(World worldIn, BlockPos pos, FluidState state, Random random) {
        if (shouldEvaporate(worldIn, pos, state)) {
            evaporate(worldIn, pos, state);
        }
        else if (shouldFreeze(worldIn, pos, state)) {
            freeze(worldIn, pos, state);
        }
        else {
            super.randomTick(worldIn, pos, state, random);
        }
    }

    @Override
    public boolean ticksRandomly() {
        return true;
    }

    @Override
    protected boolean canSourcesMultiply() {
        return false;
    }
}
