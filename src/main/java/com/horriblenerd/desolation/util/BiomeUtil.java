package com.horriblenerd.desolation.util;

import net.minecraft.util.RegistryKey;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Created by HorribleNerd on 06/09/2020
 */
public class BiomeUtil {

    public static final List<RegistryKey<Biome>> COLD_BIOMES = Arrays.asList(Biomes.COLD_OCEAN, Biomes.DEEP_COLD_OCEAN, Biomes.FROZEN_OCEAN, Biomes.DEEP_FROZEN_OCEAN, Biomes.FROZEN_RIVER);

    public static boolean isColdBiome(World worldIn, BlockPos pos) {
        Optional<RegistryKey<Biome>> biome = worldIn.func_242406_i(pos);
        return COLD_BIOMES.stream().anyMatch(key -> Objects.equals(biome, Optional.of(key)));
    }

}
