package com.horriblenerd.desolation;

import com.horriblenerd.desolation.util.BiomeUtil;
import net.minecraft.client.Minecraft;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Dimension;
import net.minecraft.world.DimensionType;
import net.minecraft.world.LightType;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.MobSpawnInfo;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.Features;
import net.minecraft.world.gen.feature.KelpFeature;
import net.minecraft.world.gen.feature.SeaGrassFeature;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.storage.IWorldInfo;
import net.minecraft.world.storage.ServerWorldInfo;
import net.minecraftforge.client.event.EntityViewRenderEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.ForgeRegistries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

import static com.horriblenerd.desolation.util.BiomeUtil.isColdBiome;

/**
 * Created by HorribleNerd on 05/09/2020
 */
@Mod(Desolation.MODID)
public class Desolation {
    // Directly reference a log4j logger.
    private static final Logger LOGGER = LogManager.getLogger();
    public static final String MODID = "desolation";

    // TODO: leaf decay
    // TODO: ice melting

    public Desolation() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        MinecraftForge.EVENT_BUS.register(this);
    }

    @SubscribeEvent
    public void onPlayerEvent(TickEvent.PlayerTickEvent event) {
        if (event.phase == TickEvent.Phase.END) {
            return;
        }
        if (event.side == LogicalSide.CLIENT) {
            return;
        }
        if (event.type != TickEvent.Type.PLAYER) {
            return;
        }
        PlayerEntity player = event.player;
        World world = player.getEntityWorld();
        if (world instanceof ServerWorld) {
            BlockPos pos = new BlockPos(player.getPositionVec());
            if (world.getLightFor(LightType.SKY, pos) - world.getSkylightSubtracted() >= 10) {
                // TODO: helmet
                // TODO: spawn prot
                // TODO: dimension check
                player.setFire(1);
            }
        }
    }

    @SubscribeEvent
    public void onFogColors(EntityViewRenderEvent.FogColors event) {
        World world = event.getInfo().getRenderViewEntity().getEntityWorld();
        if (world.func_230315_m_().func_241510_j_()) {
            BlockPos pos = event.getInfo().getBlockPos();
            boolean isDay = world.getDayTime() <= 13000;
            boolean isCold = isColdBiome(world, pos);
            if (isDay) {
                if (isCold) {
                    event.setRed(0.5F);
                    event.setGreen(0.8F);
                    event.setBlue(0.86F);
                }
                else {
                    event.setRed(1);
                    event.setGreen(0.7F);
                    event.setBlue(0.4F);
                }
            }
            else {
                event.setRed(0.5F);
                event.setGreen(0.8F);
                event.setBlue(0.86F);
            }
        }
    }

    @SubscribeEvent
    public void onFogDensity(EntityViewRenderEvent.FogDensity event) {
        World world = event.getInfo().getRenderViewEntity().getEntityWorld();
        if (world.func_230315_m_().func_241510_j_()) {
            BlockPos pos = event.getInfo().getBlockPos();
            boolean isDay = world.getDayTime() <= 13000;
            boolean isCold = isColdBiome(world, pos);
            event.setDensity(isDay ? isCold ? 0.02F : 0.02F : isCold ? 0.04F : 0.02F);
            event.setCanceled(true);
        }
    }

    public void setup(FMLCommonSetupEvent event) {
        LOGGER.debug("FMLCommonSetupEvent starting...");
//        for (Biome biome : ForgeRegistries.BIOMES) {
//            biome.func_242440_e().func_242496_b().removeIf(f -> f == Features.field_243783_I);
//            biome.func_242433_b().func_242559_a(EntityClassification.MONSTER).removeIf(s -> s.field_242588_c == EntityType.ZOMBIE);
//            biome.func_242433_b().func_242559_a(EntityClassification.MONSTER).add(new MobSpawnInfo.Spawners(EntityType.HUSK, 80, 4, 4));
//        }
    }

}
